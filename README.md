Finding signal in large volumes of data can be tricky, especially if it's based on fictional literature.
In this project I use googles n-gram data behind the Google Ngram Viewer: https://books.google.com/ngrams and http://storage.googleapis.com/books/ngrams/books/datasetsv2.html to see how language in reaction to large scale events.

Using machine learning I explore if we can tell when World War 2 is occurring and if we don't tell the algorithm when the war ends, can we reasonably tell when the war ended given only fictional literature.